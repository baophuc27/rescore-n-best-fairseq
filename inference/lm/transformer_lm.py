from typing import List

import torch
from fairseq.models.transformer_lm import TransformerLanguageModel

from inference.config.settings import settings


class TransformerLM(TransformerLanguageModel):
    
    @classmethod
    def calculate_batch_ppl(cls, model, sentences: List[str]):

        with torch.no_grad():
            scores = model.score(sentences)

        scores = list(map(lambda x: x["positional_scores"].tolist(), scores))

        return list(zip(sentences, scores))

    @classmethod
    def load_pretrained_model(cls, checkpoint_path, tokenizer="space"):
        *parent_dir, model_name = checkpoint_path.split("/")
        model_dir = "/".join(parent_dir)
        
        try:
            return cls.from_pretrained(model_dir, model_name, tokenizer=tokenizer)
        except:
            raise ValueError(f"Checkpoint: {checkpoint_path} does not exist.")
        


tranformer_lm = TransformerLM.load_pretrained_model(
    checkpoint_path=settings.CHECKPOINT_PATH, tokenizer=settings.TOKENIZER
)


def get_transformer_lm():
    return tranformer_lm
