import logging
from copy import copy


class SpecialFormatter(logging.Formatter):
    def format(self, record):
        record_copy = copy(record)
        log_level = record_copy.levelname
        message = record_copy.getMessage()
        separator = " " * (9 - len(record_copy.levelname))
        record_copy.message = log_level + ":" + separator + message
        return super().formatMessage(record_copy)


lm_logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
handler.setFormatter(SpecialFormatter())
lm_logger.addHandler(handler)
lm_logger.setLevel(logging.INFO)
