import os
from pathlib import Path
from typing import Dict, List, Optional

import yaml
from pydantic import BaseSettings
from pydantic.tools import lru_cache

config_file = "config.yml"
with open(config_file, "r") as f:
    config = yaml.load(f, Loader=yaml.FullLoader)


class Settings(BaseSettings):

    ROOT_DIR = Path(os.path.abspath(os.curdir))

    CHECKPOINT_PATH: str = config["model"]["checkpoint_path"]

    TOKENIZER: str = config["model"]["tokenizer"]

    APP_NAME: str = config["app_name"]

    def __str__(self) -> str:
        return self.CHECKPOINT_PATH


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()
