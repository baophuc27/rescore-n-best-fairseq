from fastapi import APIRouter, Depends, HTTPException

from inference.lm.transformer_lm import TransformerLM, get_transformer_lm
from inference.model.rescored_output import RescoredSentences, Sentence
from inference.model.sentences_input import SentenceInput
from starlette.status import (
    HTTP_400_BAD_REQUEST
)

router = APIRouter(
    prefix="/rescore-nbest",
    tags=["Rescore n-best"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", response_model=RescoredSentences)
async def rescore_nbest(
    text_input: SentenceInput, rescore_lm: TransformerLM = Depends(get_transformer_lm)
):
    """
    Perform n-best rescoring with pretrained neural LM

    Args:
    -------
        text_input (String, Optional): Input sentences which need to be rescored by DL LM

    Raises:
    -------
        HTTPException 400: raise when the input paremeters are not valid

    Returns:
    -------
        Optional[List[RescoredSentence]]: Rescored ppl score.
    """
    sentences = text_input.sentences
    
    try:
        rescored_ppl = TransformerLM.calculate_batch_ppl(rescore_lm, sentences=sentences)
    except:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=f"Invalid sentences: {sentences}")
    
    rescored_sents = [
        Sentence(text=sent, ppl_score=score) for sent, score in rescored_ppl
    ]

    result = RescoredSentences(id=text_input.id, rescored_sentences=rescored_sents)

    return result
