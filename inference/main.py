from fastapi import FastAPI

from .api import rescore_nbest

app = FastAPI()

app.include_router(rescore_nbest.router)


@app.get("/")
def read_root():
    return {"Hello": "World"}
