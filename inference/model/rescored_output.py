from typing import List

from pydantic import BaseModel


class Sentence(BaseModel):
    text: str
    ppl_score: List[float]


class RescoredSentences(BaseModel):
    id: str
    rescored_sentences: List[Sentence]
