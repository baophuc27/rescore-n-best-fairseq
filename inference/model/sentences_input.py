from typing import List

from pydantic import BaseModel


class SentenceInput(BaseModel):
    id: str
    sentences: List[str]
