#!/bin/bash

if [ "$1" == "--fix" ]; then
  python -m isort .&& \
  python -m black ./inference/ && \
  python -m flake8 ./inference
else
  python -m isort . --check-only && \
  python -m black ./inference/ --check && \
  python -m flake8 ./inference
fi
